package GossipSim

import scala.util.Random

abstract class Topology(var m_numNodes: Int){
	var numNodes: Int = m_numNodes
	var randGen = new Random()
	var values: Array[Int] = Array[Int]()
	var it3: Int = 0
	for(it3 <- 0 until numNodes){
		values = values :+ it3
	}
	for(it3 <- 1 until numNodes){
		var rand: Int = randGen.nextInt(it3)
		var temp: Int = values(it3)
		values(it3) = values(rand)
		values(rand) = temp
	}


	def getRandomNeighbor(id: Int): Int = {
		return 0
	}

	def getNeighbors(id: Int): Array[Int] = {
		return Array[Int](0)
	}

	def getValue(id: Int): Int = {
		return values(id)
	}
}

class LineTopology(m_numNodes: Int) extends Topology(m_numNodes){
	override def getRandomNeighbor(id: Int): Int = {
		if(id == 0) return 1;
		if(id == numNodes - 1) return numNodes - 2;
		if(id > 0 && id < numNodes)
			if(randGen.nextBoolean())
				return id + 1
			else
				return id - 1
		return -1
	}

	override def getNeighbors(id: Int): Array[Int] = {
		var Neighbors: Array[Int] = Array[Int]()
		if(id == 0){
			Neighbors = Neighbors :+ (1)
		}
		else if(id == numNodes - 1){
			Neighbors = Neighbors :+ (numNodes - 2)
		}
		else if(id > 0 && id < numNodes-1){
			Neighbors = Neighbors :+ (id - 1)
			Neighbors = Neighbors :+ (id + 1)
		}
		return Neighbors
	}
}

class FullNetTopology(m_numNodes: Int) extends Topology(m_numNodes){
	var AllMembers: Array[Int] = (0 to (m_numNodes - 1)).toArray

	override def getRandomNeighbor(id: Int): Int = {
		var rand = randGen.nextInt(m_numNodes)
		while(rand == id)
			rand = randGen.nextInt(m_numNodes)
		return rand
	}

	override def getNeighbors(id: Int): Array[Int] = {
		return Array(id, m_numNodes)
	}
}

class Grid3DTopology(m_numNodes: Int) extends Topology(m_numNodes){
	var dim: Int = 1
	while((dim*dim*dim) < numNodes)
		dim += 1

	override def getRandomNeighbor(id: Int): Int = {
		var neighbors: Array[Int] = getNeighbors(id)
		return neighbors(randGen.nextInt(neighbors.length))
	}

	override def getNeighbors(id: Int): Array[Int] = {
		var x: Int = id/(dim*dim)
		var y: Int = (id%(dim*dim))/dim
		var z: Int = id%dim

		var neighbors: Array[Int] = Array[Int]()
		if(x == 0)
			neighbors = neighbors :+ (((x+1)*dim*dim) + (y*dim) + (z))
		else if(x == (dim - 1))
			neighbors = neighbors :+ (((x-1)*dim*dim) + (y*dim) + (z))
		else{
			neighbors = neighbors :+ (((x+1)*dim*dim) + (y*dim) + (z))
			neighbors = neighbors :+ (((x-1)*dim*dim) + (y*dim) + (z))
		}

		if(y == 0)
			neighbors = neighbors :+ ((x*dim*dim) + ((y+1)*dim) + (z))
		else if(y == (dim - 1))
			neighbors = neighbors :+ ((x*dim*dim) + ((y-1)*dim) + (z))
		else{
			neighbors = neighbors :+ ((x*dim*dim) + ((y+1)*dim) + (z))
			neighbors = neighbors :+ ((x*dim*dim) + ((y-1)*dim) + (z))
		}

		if(z == 0)
			neighbors = neighbors :+ ((x*dim*dim) + (y*dim) + (z+1))
		else if(z == (dim - 1))
			neighbors = neighbors :+ ((x*dim*dim) + (y*dim) + (z-1))
		else{
			neighbors = neighbors :+ ((x*dim*dim) + (y*dim) + (z+1))
			neighbors = neighbors :+ ((x*dim*dim) + (y*dim) + (z-1))
		}
		return neighbors
	}
}

class ImperfectGrid3DTopology(m_numNodes: Int) extends Grid3DTopology(m_numNodes){

	var randNeighbors: Array[Int] = Array[Int]()
	var it = 0
	for(it <- 0 until m_numNodes){
		var rand = randGen.nextInt(m_numNodes)
		while(rand == it)
			rand = randGen.nextInt(m_numNodes)
		randNeighbors = randNeighbors :+ rand
	}


	override def getRandomNeighbor(id: Int): Int = {
		var neighbors: Array[Int] = getNeighbors(id)
		return neighbors(randGen.nextInt(neighbors.length))
	}

	override def getNeighbors(id: Int): Array[Int] = {
		return (super.getNeighbors(id) :+ randNeighbors(id))
	}
}
