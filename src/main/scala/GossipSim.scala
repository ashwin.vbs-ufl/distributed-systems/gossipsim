package GossipSim

import akka.actor._
import scala.concurrent.Await
import scala.concurrent.Future
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import com.typesafe.config._

object GossipSim {

	def test(num: Int, topology: String, algorithm: String, term: Int = 10, kill: Int = 0): Boolean = {
			var numNodes = num
			if((topology != "full") && (topology != "3D") && (topology != "line") && (topology != "imp3D"))
				System.exit(1)
			if((algorithm != "gossip") && (algorithm != "push-sum"))
				System.exit(1)
			if(numNodes < 2)
				System.exit(1)

			if(topology.contains("3D"))
			{
				var it: Int = 1
				while((it*it*it) < numNodes)
					it += 1
				numNodes = (it*it*it)
			}

		val configString: String = """akka {
			  log-dead-letters-during-shutdown = off
			  log-dead-letters = off
		}"""

			val system = ActorSystem("GossipSim", ConfigFactory.parseString(configString))
			val admin = system.actorOf(Props[Admin], "Admin")

			admin ! initlocal(numNodes, topology)
			admin ! killNodes(kill)

			implicit val timeout = Timeout(600 seconds)
			var future = admin ? kickoff(algorithm, term)
			val result = Await.result(future, timeout.duration).asInstanceOf[String]

			system.shutdown()

			if(result == "success")
				return true
			else
				return false
	}


	def main(args: Array[String]) {
		if(args(0) != "autotest"){
			var numNodes: Int = args(0).toInt
			var topology: String = args(1)
			var algorithm: String = args(2)

			println("Topology\tNumNodes\tKilled\tAlgorithm\tTerm\tUntouched\tSteps\tTime")
			test(numNodes, topology, algorithm)
		}
		else{
			println("")
			println("Topology\tNumNodes\tKilled\tAlgorithm\tTerm\tUntouched\tSteps\tTime")

// 			for(num <- 2 to 100)
// 				for(it <- 0 until 10)
// 					test(num, "line", "gossip", 100, 0)
// 			for(num <- 2 to 47)
// 				for(it <- 0 until 10)
// 					test(num*num*num, "3D", "gossip", 100, 0)
			for(num <- 0 to 9)
				for(it <- 0 until 10)
					test(100000, "3D", "gossip", 100, num * 10000)
// 			for(num <- 2 to 47)
// 				for(it <- 0 until 10)
// 					test(num*num*num, "full", "gossip", 100, 0)
//
//
// 			for(num <- 2 to 2000)
// 				for(it <- 0 until 10)
// 					test(num, "line", "push-sum", 30, 0)
			for(num <- 0 to 9)
				for(it <- 0 until 10)
					test(100000, "3D", "push-sum", 30, num * 10000)
// 			for(num <- 2 to 47)
// 				for(it <- 0 until 10)
// 					test(num*num*num, "imp3D", "push-sum", 30, 0)
// 			for(num <- 2 to 47)
// 				for(it <- 0 until 10)
// 					test(num*num*num, "full", "push-sum", 30, 0)

		}
	}
}

// object Application extends App {
// }
