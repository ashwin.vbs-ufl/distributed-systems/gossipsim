package GossipSim

import akka.actor._
import scala.util.Random
// import scala.concurrent.duration._
// import system.dispatcher
import scala.math._

case class initNode(it: Int, value: Int, topoNeighbors: Array[Int])

case class gossip(it: Int, term: Int, steps: Long)

case class gossipflood(it: Int, term: Int, toSelf: Boolean)

case class pushSum(s: Float, w: Float, term: Int, steps: Long)

class Node extends Actor{
	var id: Int = 0
	var value: Float = 0f
	var weight: Float = 1f
	var neigh: Array[ActorRef] = Array[ActorRef]()
	var recvCount: Int = 0
	var randGen = new Random()
	var admin: ActorRef = null
	var touched: Boolean = false
	var finished: Boolean = false
	var cancellable: Cancellable = null
	var fullTopo: Boolean = false
	var sizeTopo: Int = 0
	var val1: Float = 0f
	var val2: Float = 0f

	def getNeigh (): ActorRef = {
		var result: ActorRef = null
		do{
			if(fullTopo){
				var randInt: Int = randGen.nextInt(sizeTopo)
				while(randInt == id)
					randInt = randGen.nextInt(sizeTopo)
				result = context.actorFor("../Node" + randInt.toString)
			}
			else
				result = neigh(randGen.nextInt(neigh.length))
		}while(result.isTerminated == true)

		return result
	}

	def receive = {
		case initNode(it: Int, m_value: Int, topoNeighbors: Array[Int]) => {
			id = it
			value = m_value
			if(topoNeighbors(0) == id){
				fullTopo = true
				sizeTopo = topoNeighbors(1)
			}
			else
				for(actor <- topoNeighbors)
					neigh = neigh :+ context.actorFor("../Node" + actor.toString)
			admin = sender
		}

// 		case gossipflood(it: Int, term: Int, toSelf: Boolean) => {
// 			if (!touched){
// 				admin ! actorTouched()
// 				touched = true
// 			}
// 			if(toSelf)
// 			{
// 				neigh(randGen.nextInt(neigh.length)) ! gossipflood(it, term, false)
// 			}
// 			else if(recvCount < term){
// 				recvCount += 1
// 				cancellable = context.system.scheduler.schedule(0 milliseconds, 50 milliseconds, self, gossipflood(it, term, true), context.dispatcher, null)
// 			}
// 			else if(!finished){
// 				cancellable.cancel()
// 				admin ! gossipflood(it, term, false)
// 				finished =true
// 			}
// 		}

		case gossip(it: Int, term: Int, steps: Long) => {
			var nsteps = steps + 1
			if (!touched){
				admin ! actorTouched()
				touched = true
			}

			if(recvCount > term)
				admin ! gossip(it, term, nsteps)
			else{
				getNeigh () ! gossip(it, term, nsteps)
				recvCount += 1
			}
		}

		case pushSum(s: Float, w: Float, term: Int, steps: Long) => {
			var nsteps = steps + 1
			if (!touched){
				admin ! actorTouched()
				touched = true
			}

			value = (s + value)/2
			weight = (w + weight)/2

			var val3: Float = value/weight
			if((abs(val3-val2) < pow(10, (-1*term))) && ((abs(val2-val1) < pow(10, (-1*term)))))
				admin ! pushSum(value, weight, term, nsteps)
			else{
				getNeigh () ! pushSum(value, weight, term, nsteps)
				val1 = val2
				val2 = val3
			}
// 			if(w<1f)
// 				println((((s/w)*pow(10, term - 7)) - floor(((s/w)*pow(10, term - 7)))) * 10000000)

		}
	}
}
