package GossipSim

import akka.actor._
import scala.util.Random

case class initlocal(numNodes: Int, topology: String)

case class killNodes(numNodes: Int)

case class kickoff(algorithm: String, term: Int = 10)

case class actorTouched()

// case class result(num:Int, untouched: Int, steps: Long, time: Long, avg: Float)

class Admin extends Actor{
	var topo: Topology = null
	var topoStr: String = ""
	var num: Int = 0
	var currentGossip: Int = 0
	var untouchedNodes: Int = 0
	var unfinishedNodes: Int = 0
	var startTime: Long = 0
	var main: ActorRef = null
	//var killed: Int = 0

	def receive = {
		case initlocal(numNodes: Int, topology: String) => {
			num = numNodes
			topoStr = topology
			untouchedNodes = num
			if(topology == "full")
				topo = new FullNetTopology(numNodes)
			else if(topology == "3D")
				topo = new Grid3DTopology(numNodes)
			else if(topology == "line")
				topo = new LineTopology(numNodes)
			else if(topology == "imp3D")
				topo = new ImperfectGrid3DTopology(numNodes)
			var it = 0
			for(it <- 0 until numNodes)
				context.actorOf(Props[Node], "Node" + it.toString)
			for(it <- 0 until numNodes)
				context.child("Node" + it.toString).getOrElse(null) ! initNode(it, topo.getValue(it), topo.getNeighbors(it))
		}

		case killNodes(numNodes: Int) => {
			if(numNodes > 0 && numNodes < num){
				var array: Array[Boolean] = Array[Boolean]()
				for(it <- 0 until num)
					array = array :+ true
				untouchedNodes = untouchedNodes-numNodes
				var it: Int = 0
				var randGen = new Random()
				var rand: Int = 0
				for(it <- 0 until numNodes){
					do{
						rand = randGen.nextInt(num)
					}while(rand == num/2 || !array(rand))
					context.stop(context.child("Node" + rand.toString).getOrElse(null))
					array(rand) = false
				}
			}
		}

		case kickoff(algorithm: String, term: Int) => {
			main = sender
			startTime = System.currentTimeMillis
			if(algorithm == "gossip"){
				context.child("Node" + (num/2).toString).getOrElse(null) ! gossip(currentGossip, term, 0)
				currentGossip += 1
			}
			else if(algorithm == "push-sum")
				context.child("Node" + (num/2).toString).getOrElse(null) ! pushSum(0, 0, term, 0)
		}

		case actorTouched() => {
			untouchedNodes = untouchedNodes - 1
		}

		case gossip(it: Int, term: Int, steps: Long) => {
			var killed: Int = 0
			for(it <- 0 until num)
				if(context.child("Node" + it.toString).getOrElse(null) == null)
					killed += 1
			println(topoStr + "\t\t" + num.toString + "\t\t" + killed.toString + "\tgossip\t\t" + term + "\t" + untouchedNodes.toString + "\t\t" + steps.toString + "\t" + (System.currentTimeMillis - startTime).toString + " ms")
			main ! "success"
		}

// 		case gossipflood(it: Int, term: Int, toSelf: Boolean) => {
// 			println("Gossip algorithm finished in " + (System.currentTimeMillis - startTime).toString + " ms")
// 			println("Number of nodes untouched: " + untouchedNodes.toString)
// 			main ! "success"
// 		}

		case pushSum(s: Float, w: Float, term: Int, steps: Long) => {
			var killed: Int = 0
			for(it <- 0 until num)
				if(context.child("Node" + it.toString).getOrElse(null) == null)
					killed += 1
			println(topoStr + "\t\t" + num.toString + "\t\t" + killed.toString + "\tpush-sum\t" + term + "\t" + untouchedNodes.toString + "\t\t" + steps.toString + "\t" + (System.currentTimeMillis - startTime).toString + " ms\tAverage Exp/Calc: " + (num-1).toFloat/2 + "/" + (s/w).toString)
			main ! "success"
		}
	}
}
