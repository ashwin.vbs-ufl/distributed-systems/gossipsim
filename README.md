Description:

Simulates gossip and push-sum algorithm using Akka framework in Scala. Both algorithms deal with dissemination of information in a non-regular mesh network.

Gossip Algorithm:

Algorithm deals with a scenario where a single message has to be disseminated across the entire network. The message takes a random walk within the network. The walk is terminated when it reaches any node a certain number of times – This limit is preset in the scenario.

Push-Sum Algorithm:

The algorithm deals with a scenario where the sum of quantities held by the different nodes has to be calculated. The push-sum message takes a random walk within the network. At each node, v and w values in the message are added to the corresponding values of the node and halved. This value is stored both in the message (that is passed along in the random walk) and the node. The walk is terminated when the change in value of v/w at any node in three consecutive falls within a preset threshold.

Prerequisites:

Sbt and Scala

Instructions to run the program:

* sbt build at the project root.

* sbt “run <parameters>” at the project root will execute the program.

Parameters can be

1. auto test
will run a suite of tests cycling through the algorithms and number of nodes.

2. <numNodes> <topology> <algorithm>
Will run that exact scenario. numNodes is an integer value with number of nodes, topology should be either line, 3D, imp3D or full and algorithm should be either gossip or push-sum.